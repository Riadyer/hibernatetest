/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.james.hibernate.service;
import com.james.hibernate.entity.Person;
import java.util.List;
/**
 *
 * @author James Riady
 */
public interface ServiceDao {
    public void save(Person p);
        
    public void delete(Person p);
    
    //untuk mengambil 1 record dengan primary key
    public Person getPersonById(Integer id);
    //untuk mengambil semua record dari 1 table
    public List<Person> getPersons();
}
