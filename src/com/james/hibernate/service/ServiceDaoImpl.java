/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.james.hibernate.service;

import com.james.hibernate.entity.Person;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author James Riady
 */
public class ServiceDaoImpl implements ServiceDao{

    private Transaction tx = null;
    
    public Session getSession(){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        tx = session.beginTransaction();
        return session;
    }
    
    @Override
    public void save(Person p) {
        getSession().saveOrUpdate(p);
        tx.commit();
    }

    @Override
    public void delete(Person p) {
        getSession().delete(p);
        tx.commit();
    }

    @Override
    public Person getPersonById(Integer id) {
        Person p = (Person) getSession().get(Person.class, id);
        tx.commit();
        return p;
    }

    @Override
    public List<Person> getPersons() {
       List<Person> list = getSession().createQuery("from Person").list();
       tx.commit();
       return list;
    }
    
}
