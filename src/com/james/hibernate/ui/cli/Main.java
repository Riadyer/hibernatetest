/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.james.hibernate.ui.cli;

import com.james.hibernate.entity.Person;
import com.james.hibernate.service.ServiceDao;
import com.james.hibernate.service.ServiceDaoImpl;
import java.util.Date;
import java.util.List;

/**
 *
 * @author James Riady
 */
public class Main {
private static ServiceDao service;


public static void Create() {
    Person p = new Person();
       p.setId(5);
       p.setPersonname("sutandi");
       p.setGender("Male");
       p.setBirthday(new Date());
       p.setEmail("Zukarine97@gmail.com");
       service.save(p);
       System.exit(0);
}
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        service = new ServiceDaoImpl();
        
       // create record
       Create();
       //read by id
//       Person p2 = service.getPersonById(1);
//        System.out.println(p2.getPersonname());
     
       //update by id
//       p2.setPersonname("Roderick");
//        System.out.println(p2.getPersonname());
//        System.exit(0);
       //read all
//       List<Person> listPerson = service.getPersons();
//       for (Person person : listPerson){
//           System.out.println("Persname:    " + person.getPersonname());
//       }
       //delete id
//       Person p4 = service.getPersonById(1);
//       service.delete(p4);
       
    }
    
}
